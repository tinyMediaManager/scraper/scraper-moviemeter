MovieMeter.nl scraper for tinyMediaManager
===========================================

for use in the app tinyMediaManager (https://www.tinymediamanager.org)

## Issues
All issues for tinyMediaManager and its components are managed at https://github.com/tinyMediaManager/tinyMediaManager/issues
